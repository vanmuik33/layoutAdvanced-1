// Collapse navbar
function myFunction() {
  if (
    $("#myNavbar").hasClass("responsive") == true &&
    $(window).scrollTop() < 10
  ) {
    $("#header").css("background-color", "transparent");
  } else $("#header").css("background-color", "#f38181");
  var x = document.getElementById("myNavbar");
  if (x.className === "navbar") {
    x.className += " responsive";
    $(".logo").addClass("border");
  } else {
    x.className = "navbar";
    $(".logo").removeClass("border");
  }
}

$(window).bind("scroll", function () {
  if ($(window).scrollTop() >= 10) {
    $("#header").css("background-color", "#f38181");
  } else {
    if ($("#myNavbar").hasClass("responsive") == false) {
      $("#header").css("background-color", "transparent");
    }
  }
});

$(document).ready(function () {
  if ($(window).scrollTop() >= 10) {
    $("#header").css("background-color", "#f38181");
  }
});

//============================================>> search-button

function openSearch() {
  $('#myOverlay').show();
}

function closeSearch() {
  $('#myOverlay').hide();
}

//============================================>> facts

var isInViewport = function (elem) {
  var bounding = elem.getBoundingClientRect();
  return (
    // bounding.top >= 0 && bounding.left >= 0
    // bounding.bottom <=
    // (window.innerHeight || document.documentElement.clientHeight)
    // bounding.right <=
    // (window.innerWidth || document.documentElement.clientWidth)
    bounding.top <=
    (window.innerHeight || document.documentElement.clientHeight)
  );
};

$(document).ready(function () {
  var pos = document.querySelector("#facts");
  if (isInViewport(pos)) {
    $(".counter-number").addClass("number-show");
    $(".counter-number").countTo();
  } else {
    $(window).scroll(function (event) {
      if (isInViewport(pos)) {
        $(function () {
          if ($(".counter-number").hasClass("number-show")) {
            $(window).off("scrool");
          } else {
            $(".counter-number").addClass("number-show");
            $(".counter-number").countTo();
          }
        });
      }
    });
  }
});

//===================================>> ScrollSpy

$(document).ready(function () {
  $("nav").stickynav();
});

// ==============================>> slider

$(".slider").slick({
  infinite: true,
  arrows: false,
  dots: false,
  autoplay: false,
  speed: 1500,
  slidesToShow: 1,
  slidesToScroll: 1,
  fade: true,
  cssEase: "linear",
});

//ticking machine
var percentTime;
var tick;
var time = 1;
var progressBarIndex = 0;

$(".progressBarContainer .progressBar").each(function (index) {
  var progress = "<div class='inProgress inProgress" + index + "'></div>";
  $(this).html(progress);
});

function startProgressbar() {
  resetProgressbar();
  percentTime = 0;
  tick = setInterval(interval, 10);
}

function interval() {
  if (
    $(
      '.slider .slick-track div[data-slick-index="' + progressBarIndex + '"]'
    ).attr("aria-hidden") === "true"
  ) {
    progressBarIndex = $('.slider .slick-track div[aria-hidden="false"]').data(
      "slickIndex"
    );
    startProgressbar();
  } else {
    percentTime += 1 / (time + 5);
    $(".inProgress" + progressBarIndex).css({
      width: percentTime + "%",
    });
    if (percentTime >= 100) {
      $(".progressBarContainer div").removeClass("inActive");
      $(".single-item").slick("slickNext");
      progressBarIndex++;
      $(
        '.progressBarContainer span[data-slick-index="' +
          progressBarIndex-- +
          '"]'
      )
        .parent()
        .addClass("inActive");
      if (progressBarIndex > 3) {
        progressBarIndex = 0;
      }
    }
  }
}

function resetProgressbar() {
  $('.progressBarContainer span[data-slick-index="' + progressBarIndex + '"]')
    .parent()
    .addClass("inActive");

  $(".inProgress").css({
    width: 0 + "%",
  });
  clearInterval(tick);
}
startProgressbar();
// End ticking machine

// $(".progressBarContainer div").click(function () {
//   var currentIndex = $(".single-item").slick("slickCurrentSlide");
//   if ($(this).attr("class") == "inActive") {
//     startProgressbar();
//   } else {
//     clearInterval(tick);
//     var goToThisIndex = $(this).find("span").data("slickIndex");
//     $(".single-item").slick("slickGoTo", goToThisIndex, false);
//     startProgressbar();
//     $('.progressBarContainer span[data-slick-index="' + currentIndex + '"]')
//       .parent()
//       .removeClass("inActive");
//   }
// });

$(".progressBarContainer div").click(function () {
  var currentIndex = $('.slider').slick('slickCurrentSlide');
  // alert(currentIndex);
  var goToThisIndex = $(this).find('span').data('slickIndex');
  // alert(goToThisIndex);
  // $('.slider').slick('slickGoTo', goToThisIndex, false);
});


// ===========================>> smooth scrolling
// $(document).ready(function() {
//     // Add smooth scrolling to all links
//     $("a").on('click', function(event) {

//         // Make sure this.hash has a value before overriding default behavior
//         if (this.hash !== "") {
//             // Prevent default anchor click behavior
//             event.preventDefault();

//             // Store hash
//             var hash = this.hash;

//             // Using jQuery's animate() method to add smooth page scroll
//             // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
//             $('html, body').animate({
//                 scrollTop: $(hash).offset().top
//             }, 500, function() {

//                 // Add hash (#) to URL when done scrolling (default click behavior)
//                 window.location.hash = hash;
//             });
//         } // End if
//     });
// });

// ==============================>> fancybox

$('[data-fancybox="instagram-image"]').fancybox({
  buttons: ["slideShow", "zoom", "fullScreen", "close"],
  thumbs: {
    autoStart: true,
  },
  loop: true,
});
